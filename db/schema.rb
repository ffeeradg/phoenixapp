# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_09_28_043908) do

  create_table "brands", force: :cascade do |t|
    t.string "codigo_Bra"
    t.string "nombre_Bra"
    t.integer "maker_id"
    t.boolean "estado_Bra", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["maker_id"], name: "index_brands_on_maker_id"
  end

  create_table "cm_categories", force: :cascade do |t|
    t.string "codigo_CM"
    t.string "nombre_CM"
    t.boolean "estado_CM", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "cp_category_id"
    t.index ["cp_category_id"], name: "index_cm_categories_on_cp_category_id"
  end

  create_table "companies", force: :cascade do |t|
    t.string "codigo_Com"
    t.string "nombre_Com"
    t.text "descripcion_Com"
    t.boolean "estado_Com", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cp_categories", force: :cascade do |t|
    t.string "codigo_CP"
    t.string "nombre_CP"
    t.boolean "estado_CP"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cycles", force: :cascade do |t|
    t.string "codigo_Cyc"
    t.string "nombre_Cyc"
    t.boolean "estado_Cyc", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "departments", force: :cascade do |t|
    t.string "codigo_Dep"
    t.string "nombre_Dep"
    t.text "descripcion_Dep"
    t.boolean "estado_Dep", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "locations", force: :cascade do |t|
    t.string "codigo_Loc"
    t.string "nombre_Loc"
    t.text "descripcion_Loc"
    t.integer "subsidiary_id"
    t.boolean "estado_Loc", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["subsidiary_id"], name: "index_locations_on_subsidiary_id"
  end

  create_table "makers", force: :cascade do |t|
    t.string "codigo_Mak"
    t.string "nombre_Mak"
    t.boolean "estado_Mak", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "models", force: :cascade do |t|
    t.string "codigo_Mod"
    t.string "nombre_Mod"
    t.integer "maker_id"
    t.integer "brand_id"
    t.integer "cm_category_id"
    t.boolean "estado_Mod", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["brand_id"], name: "index_models_on_brand_id"
    t.index ["cm_category_id"], name: "index_models_on_cm_category_id"
    t.index ["maker_id"], name: "index_models_on_maker_id"
  end

  create_table "municipalities", force: :cascade do |t|
    t.string "codigo_Mun"
    t.string "nombre_Mun"
    t.text "descripcion_Mun"
    t.integer "department_id"
    t.boolean "estado_Mun", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["department_id"], name: "index_municipalities_on_department_id"
  end

  create_table "order_states", force: :cascade do |t|
    t.string "codigo_Ost"
    t.string "nombre_Ost"
    t.text "descripcion_Ost"
    t.boolean "estado_Ost", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "products", force: :cascade do |t|
    t.string "codigo_Prod"
    t.text "descripcion_Prod"
    t.integer "cantidad_Prod"
    t.boolean "estado_Prod"
    t.integer "model_id"
    t.integer "location_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["location_id"], name: "index_products_on_location_id"
    t.index ["model_id"], name: "index_products_on_model_id"
  end

  create_table "profiles", force: :cascade do |t|
    t.string "codigo_Pro"
    t.string "nombre_Pro"
    t.string "apellido_Pro"
    t.string "telefono_Pro"
    t.string "direccion_Pro"
    t.integer "user_id"
    t.integer "rol_id"
    t.integer "subsidiary_id"
    t.boolean "estado_Pro", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "department_id"
    t.integer "municipality_id"
    t.integer "company_id"
    t.index ["company_id"], name: "index_profiles_on_company_id"
    t.index ["department_id"], name: "index_profiles_on_department_id"
    t.index ["municipality_id"], name: "index_profiles_on_municipality_id"
    t.index ["rol_id"], name: "index_profiles_on_rol_id"
    t.index ["subsidiary_id"], name: "index_profiles_on_subsidiary_id"
    t.index ["user_id"], name: "index_profiles_on_user_id"
  end

  create_table "rols", force: :cascade do |t|
    t.string "codigo_Rol"
    t.string "nombre_Rol"
    t.text "descripcion_Rol"
    t.boolean "estado_Rol", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "service_states", force: :cascade do |t|
    t.string "codigo_Sta"
    t.string "nombre_Sta"
    t.text "descripcion_Sta"
    t.boolean "estado_Sta", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "service_types", force: :cascade do |t|
    t.string "codigo_Typ"
    t.string "nombre_Typ"
    t.text "descripcion_Typ"
    t.boolean "estado_Typ", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "subsidiaries", force: :cascade do |t|
    t.string "codigo_Sub"
    t.integer "company_id"
    t.string "nombre_Sub"
    t.string "telefono_Sub"
    t.string "direccion_Sub"
    t.integer "department_id"
    t.integer "municipality_id"
    t.boolean "estado_Sub"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["company_id"], name: "index_subsidiaries_on_company_id"
    t.index ["department_id"], name: "index_subsidiaries_on_department_id"
    t.index ["municipality_id"], name: "index_subsidiaries_on_municipality_id"
  end

  create_table "tasks", force: :cascade do |t|
    t.string "codigo_Tas"
    t.string "nombre_Tas"
    t.text "descripcion_Tas"
    t.integer "cycle_id"
    t.boolean "estado_Tas"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["cycle_id"], name: "index_tasks_on_cycle_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "nombre_Use"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
