class CreateServiceTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :service_types do |t|
      t.string :codigo_Typ
      t.string :nombre_Typ
      t.text :descripcion_Typ
      t.boolean :estado_Typ

      t.timestamps
    end
  end
end
