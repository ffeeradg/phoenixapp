class CreateCpCategories < ActiveRecord::Migration[5.2]
  def change
    create_table :cp_categories do |t|
      t.string :codigo_CP
      t.string :nombre_CP
      t.boolean :estado_CP

      t.timestamps
    end
  end
end
