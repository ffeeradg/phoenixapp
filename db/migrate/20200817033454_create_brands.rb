class CreateBrands < ActiveRecord::Migration[5.2]
  def change
    create_table :brands do |t|
      t.string :codigo_Bra
      t.string :nombre_Bra
      t.references :maker, foreign_key: true
      t.boolean :estado_Bra

      t.timestamps
    end
  end
end
