class CreateOrderStates < ActiveRecord::Migration[5.2]
  def change
    create_table :order_states do |t|
      t.string :codigo_Ost
      t.string :nombre_Ost
      t.text :descripcion_Ost
      t.boolean :estado_Ost

      t.timestamps
    end
  end
end
