class CreateTasks < ActiveRecord::Migration[5.2]
  def change
    create_table :tasks do |t|
      t.string :codigo_Tas
      t.string :nombre_Tas
      t.text :descripcion_Tas
      t.references :cycle, foreign_key: true
      t.boolean :estado_Tas

      t.timestamps
    end
  end
end
