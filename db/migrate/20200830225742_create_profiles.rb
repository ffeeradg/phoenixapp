class CreateProfiles < ActiveRecord::Migration[5.2]
  def change
    create_table :profiles do |t|
      t.string :codigo_Pro
      t.string :nombre_Pro
      t.string :apellido_Pro
      t.string :telefono_Pro
      t.string :direccion_Pro
      t.references :user, foreign_key: true
      t.references :rol, foreign_key: true
      t.references :subsidiary, foreign_key: true
      t.boolean :estado_Loc

      t.timestamps
    end
  end
end
