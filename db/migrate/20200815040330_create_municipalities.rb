class CreateMunicipalities < ActiveRecord::Migration[5.2]
  def change
    create_table :municipalities do |t|
      t.string :codigo_Mun
      t.string :nombre_Mun
      t.text :descripcion_Mun
      t.references :department, foreign_key: true
      t.boolean :estado_Mun

      t.timestamps
    end
  end
end
