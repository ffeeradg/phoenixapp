class CreateServiceStates < ActiveRecord::Migration[5.2]
  def change
    create_table :service_states do |t|
      t.string :codigo_Sta
      t.string :nombre_Sta
      t.text :descripcion_Sta
      t.boolean :estado_Sta

      t.timestamps
    end
  end
end
