class CreateCompanies < ActiveRecord::Migration[5.2]
  def change
    create_table :companies do |t|
      t.string :codigo_Com
      t.string :nombre_Com
      t.text :Descripcion_Com
      t.boolean :estado_Com

      t.timestamps
    end
  end
end
