class AddMunicipalityToProfiles < ActiveRecord::Migration[5.2]
  def change
    add_reference :profiles, :municipality, foreign_key: true
  end
end
