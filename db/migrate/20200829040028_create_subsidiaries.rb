class CreateSubsidiaries < ActiveRecord::Migration[5.2]
  def change
    create_table :subsidiaries do |t|
      t.string :codigo_Sub
      t.references :company, foreign_key: true
      t.string :nombre_Sub
      t.string :telefono_Sub
      t.string :direccion_Sub
      t.references :department, foreign_key: true
      t.references :municipality, foreign_key: true
      t.boolean :estado_Sub

      t.timestamps
    end
  end
end
