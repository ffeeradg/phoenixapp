class CreateCmCategories < ActiveRecord::Migration[5.2]
  def change
    create_table :cm_categories do |t|
      t.string :codigo_CM
      t.string :nombre_CM
      t.boolean :estado_CM

      t.timestamps
    end
  end
end
