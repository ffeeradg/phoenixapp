class CreateDepartments < ActiveRecord::Migration[5.2]
  def change
    create_table :departments do |t|
      t.string :codigo_Dep
      t.string :nombre_Dep
      t.text :descripcion_Dep
      t.boolean :estado_Dep

      t.timestamps
    end
  end
end
