class CreateCycles < ActiveRecord::Migration[5.2]
  def change
    create_table :cycles do |t|
      t.string :codigo_Cyc
      t.string :nombre_Cyc
      t.boolean :estado_Cyc

      t.timestamps
    end
  end
end
