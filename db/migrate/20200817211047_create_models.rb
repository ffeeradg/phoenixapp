class CreateModels < ActiveRecord::Migration[5.2]
  def change
    create_table :models do |t|
      t.string :codigo_Mod
      t.string :nombre_Mod
      t.references :maker, foreign_key: true
      t.references :brand, foreign_key: true
      t.references :cm_category, foreign_key: true
      t.boolean :estado_Mod

      t.timestamps
    end
  end
end
