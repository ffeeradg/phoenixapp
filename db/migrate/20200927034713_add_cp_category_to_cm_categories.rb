class AddCpCategoryToCmCategories < ActiveRecord::Migration[5.2]
  def change
    add_reference :cm_categories, :cp_category, foreign_key: true
  end
end
