class CreateMakers < ActiveRecord::Migration[5.2]
  def change
    create_table :makers do |t|
      t.string :codigo_Mak
      t.string :nombre_Mak
      t.boolean :estado_Mak

      t.timestamps
    end
  end
end
