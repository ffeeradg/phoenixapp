class CreateRols < ActiveRecord::Migration[5.2]
  def change
    create_table :rols do |t|
      t.string :codigo_Rol
      t.string :nombre_Rol
      t.text :descripcion_Rol
      t.boolean :estado_Rol

      t.timestamps
    end
  end
end
