class CreateLocations < ActiveRecord::Migration[5.2]
  def change
    create_table :locations do |t|
      t.string :codigo_Loc
      t.string :nombre_Loc
      t.text :descripcion_Loc
      t.references :subsidiary, foreign_key: true
      t.boolean :estado_Loc

      t.timestamps
    end
  end
end
