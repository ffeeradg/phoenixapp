class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.string :codigo_Prod
      t.text :descripcion_Prod
      t.integer :cantidad_Prod
      t.boolean :estado_Prod
      t.references :model, foreign_key: true
      t.references :location, foreign_key: true

      t.timestamps
    end
  end
end
