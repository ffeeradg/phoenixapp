class ServiceType < ApplicationRecord

  def self.search(search, page)  
      where("codigo_Typ LIKE ? OR nombre_Typ LIKE ?",
      "%#{search}%", "%#{search}%").paginate(page: page, per_page: 5).order("codigo_TYp")     
  end

  def name
		self.nombre_Typ
  end

end
