class Location < ApplicationRecord
  belongs_to :subsidiary

  def self.search(search, page)  
      where("codigo_Loc LIKE ? OR nombre_Loc LIKE ?",
      "%#{search}%", "%#{search}%").paginate(page: page, per_page: 5).order("codigo_Loc")     
  end

  def name
	self.nombre_Loc
  end

end
