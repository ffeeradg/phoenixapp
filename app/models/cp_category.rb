class CpCategory < ApplicationRecord

  has_many :cm_categories

  def self.search(search, page)  
      where("codigo_CP LIKE ? OR nombre_CP LIKE ?",
      "%#{search}%", "%#{search}%").paginate(page: page, per_page: 5).order("codigo_CP")     
  end

  def name
	self.nombre_CP
  end

end
