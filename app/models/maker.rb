class Maker < ApplicationRecord

  has_many :brands
  has_many :models
		
  def self.search(search, page)  
      where("codigo_Mak LIKE ? OR nombre_Mak LIKE ?",
      "%#{search}%", "%#{search}%").paginate(page: page, per_page: 5).order("codigo_Mak")     
  end
	
	def name
	  self.nombre_Mak
	end

end
