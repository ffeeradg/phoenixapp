class Company < ApplicationRecord
  
  has_many :subsidiaries
  has_many :profiles

  def self.search(search, page)  
      where("codigo_Com LIKE ? OR nombre_Com LIKE ?",
      "%#{search}%", "%#{search}%").paginate(page: page, per_page: 5).order("codigo_Com")     
  end

  def name
    self.nombre_Com
  end

end
