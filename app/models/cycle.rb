class Cycle < ApplicationRecord

  has_many :tasks

  def self.search(search, page)  
      where("codigo_Cyc LIKE ? OR nombre_Cyc LIKE ?",
      "%#{search}%", "%#{search}%").paginate(page: page, per_page: 5).order("codigo_Cyc")     
  end

  def name
    self.nombre_Cyc
  end

end
