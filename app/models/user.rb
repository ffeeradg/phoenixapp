class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :timeoutable

  has_one :profile, dependent: :destroy

  after_create :set_profile

  def set_profile
  	self.profile = Profile.create(user_id: "<%= current_user.email %>", rol_id: "1", department_id: "1", municipality_id: "1", company_id: "2", subsidiary_id: "4", estado_Pro: "1")
  end

  def name
    self.email
  end

end
