class OrderState < ApplicationRecord

  def self.search(search, page)  
      where("codigo_Ost LIKE ? OR nombre_Ost LIKE ?",
      "%#{search}%", "%#{search}%").paginate(page: page, per_page: 5).order("codigo_Ost")     
  end

  def name
	self.nombre_Ost
  end

end
