class Model < ApplicationRecord
  belongs_to :maker
  belongs_to :brand
  belongs_to :cm_category

  def self.search(search, page)  
      where("codigo_Mod LIKE ? OR nombre_Mod LIKE ?",
      "%#{search}%", "%#{search}%").paginate(page: page, per_page: 5).order("codigo_Mod")     
  end

	def name
	  self.nombre_Mod
	end

end
