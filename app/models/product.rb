class Product < ApplicationRecord
  belongs_to :model
  belongs_to :location

  def self.search(search, page)  
      where("codigo_Prod LIKE ?",
      "%#{search}%").paginate(page: page, per_page: 5).order("codigo_Prod")     
  end

  def name
    self.codigo_Prod
  end

end
