class ServiceState < ApplicationRecord

  def self.search(search, page)  
      where("codigo_Sta LIKE ? OR nombre_Sta LIKE ?",
      "%#{search}%", "%#{search}%").paginate(page: page, per_page: 5).order("codigo_Sta")     
  end

  def name
	self.nombre_Sta
  end

end
