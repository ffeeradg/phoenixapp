class CmCategory < ApplicationRecord

  belongs_to :cp_category

  def self.search(search, page)  
      where("codigo_CM LIKE ? OR nombre_CM LIKE ?",
      "%#{search}%", "%#{search}%").paginate(page: page, per_page: 5).order("codigo_CM")     
  end

	def name
		self.nombre_CM
	end

end
