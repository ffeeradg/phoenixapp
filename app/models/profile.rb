class Profile < ApplicationRecord
  belongs_to :user
  belongs_to :rol
  belongs_to :department
  belongs_to :municipality
  belongs_to :company
  belongs_to :subsidiary

  def self.search(search, page)  
      where("codigo_Pro LIKE ? OR nombre_Pro LIKE ? OR apellido_Pro LIKE ?",
      "%#{search}%", "%#{search}%", "%#{search}%").paginate(page: page, per_page: 5).order("codigo_Pro")     
  end

  def name
    self.nombre_Pro
  end

end
