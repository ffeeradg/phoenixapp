class Municipality < ApplicationRecord

  belongs_to :department
  has_many :profiles

  def self.search(search, page)  
      where("codigo_Mun LIKE ? OR nombre_Mun LIKE ?",
      "%#{search}%", "%#{search}%").paginate(page: page, per_page: 5).order("codigo_Mun")     
  end

  def name
	self.nombre_Mun
  end

end
