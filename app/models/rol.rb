class Rol < ApplicationRecord
  
  has_many :profiles

  def self.search(search, page)  
      where("codigo_Rol LIKE ? OR nombre_Rol LIKE ?",
      "%#{search}%", "%#{search}%").paginate(page: page, per_page: 5).order("codigo_Rol")     
  end
  
  def name
    self.nombre_Rol
  end

end
