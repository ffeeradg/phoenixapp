class Department < ApplicationRecord

  has_many :municipalities
  has_many :subsidiaries
  has_many :profiles

  def self.search(search, page)  
      where("codigo_Dep LIKE ? OR nombre_Dep LIKE ?",
      "%#{search}%", "%#{search}%").paginate(page: page, per_page: 5).order("codigo_Dep")     
  end

  def name
	self.nombre_Dep
  end

end
