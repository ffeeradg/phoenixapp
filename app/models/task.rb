class Task < ApplicationRecord

  belongs_to :cycle

  def self.search(search, page)  
      where("codigo_Tas LIKE ? OR nombre_Tas LIKE ?",
      "%#{search}%", "%#{search}%").paginate(page: page, per_page: 5).order("codigo_Tas")     
  end

  def name
    self.nombre_Tas
  end
  
end
