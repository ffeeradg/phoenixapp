class Subsidiary < ApplicationRecord
  belongs_to :company
  belongs_to :department
  belongs_to :municipality
  has_many :locations
  has_many :profiles

  def self.search(search, page)  
      where("codigo_Sub LIKE ? OR nombre_Sub LIKE ?",
      "%#{search}%", "%#{search}%").paginate(page: page, per_page: 5).order("codigo_Sub")     
  end

  def name
	self.nombre_Sub
  end

end
