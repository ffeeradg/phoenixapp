class Brand < ApplicationRecord
	
  belongs_to :maker
  has_many :models

  def self.search(search, page)  
      where("codigo_Bra LIKE ? OR nombre_Bra LIKE ?",
      "%#{search}%", "%#{search}%").paginate(page: page, per_page: 5).order("codigo_Bra")     
  end

	def name
		self.nombre_Bra
	end

end
