class CpCategoriesController < AppController
  before_action :set_cp_category, only: [:show, :edit, :update, :destroy]

  # GET /cp_categories
  # GET /cp_categories.json
  def index
    @cp_categories = CpCategory.search(params[:search], params[:page])
  end

  # GET /cp_categories/1
  # GET /cp_categories/1.json
  def show
  end

  # GET /cp_categories/new
  def new
    @cp_category = CpCategory.new
  end

  # GET /cp_categories/1/edit
  def edit
  end

  # POST /cp_categories
  # POST /cp_categories.json
  def create
    @cp_category = CpCategory.new(cp_category_params)

    respond_to do |format|
      if @cp_category.save
        format.html { redirect_to cp_categories_url, notice: 'Categoria registrada exitosamente.' }
        format.json { render :show, status: :created, location: @cp_category }
      else
        format.html { render :new }
        format.json { render json: @cp_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cp_categories/1
  # PATCH/PUT /cp_categories/1.json
  def update
    respond_to do |format|@cp_categories
      if @cp_category.update(cp_category_params)
        format.html { redirect_to cp_categories_url, notice: 'Categoria actualizada exitosamente.' }
        format.json { render :show, status: :ok, location: @cp_category }
      else
        format.html { render :edit }
        format.json { render json: @cp_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cp_categories/1
  # DELETE /cp_categories/1.json
  def destroy
    @cp_category.destroy
    respond_to do |format|
      format.html { redirect_to cp_categories_url, notice: 'Categoria eliminada exitosamente.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cp_category
      @cp_category = CpCategory.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def cp_category_params
      params.require(:cp_category).permit(:codigo_CP, :nombre_CP, :estado_CP)
    end
end
