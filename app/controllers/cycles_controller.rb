class CyclesController < AppController
  before_action :set_cycle, only: [:show, :edit, :update, :destroy]

  # GET /cycles
  # GET /cycles.json
  def index
    if current_user.profile.rol.nombre_Rol == "SUPER USUARIOS"
      @cycles = Cycle.search(params[:search], params[:page])
    else  
      @cycles = Cycle.search(params[:search], params[:page])
                        .order(nombre_Cyc: :desc)
                        .where(estado_Cyc: "1")
    end 
  end

  # GET /cycles/1
  # GET /cycles/1.json
  def show
  end

  # GET /cycles/new
  def new
    @cycle = Cycle.new
  end

  # GET /cycles/1/edit
  def edit
  end

  # POST /cycles
  # POST /cycles.json
  def create
    @cycle = Cycle.new(cycle_params)

    respond_to do |format|
      if @cycle.save
        format.html { redirect_to cycles_url, notice: 'Ciclo resgistrado exitosamente.' }
        format.json { render :show, status: :created, location: @cycle }
      else
        format.html { render :new }
        format.json { render json: @cycle.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cycles/1
  # PATCH/PUT /cycles/1.json
  def update
    respond_to do |format|
      if @cycle.update(cycle_params)
        format.html { redirect_to cycles_url, notice: 'Ciclo actualizado exitosamente.' }
        format.json { render :show, status: :ok, location: @cycle }
      else
        format.html { render :edit }
        format.json { render json: @cycle.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cycles/1
  # DELETE /cycles/1.json
  def destroy
    if current_user.profile.rol.nombre_Rol == "SUPER USUARIOS"
      @cycle.destroy
    else
        @cycle.update_column :estado_Cyc, '0'
    end
    respond_to do |format|
      format.html { redirect_to cycles_url, notice: 'Ciclo eliminado exitosamente.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cycle
      @cycle = Cycle.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def cycle_params
      params.require(:cycle).permit(:codigo_Cyc, :nombre_Cyc, :estado_Cyc)
    end
end
