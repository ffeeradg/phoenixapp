class OrderStatesController < AppController
  before_action :set_order_state, only: [:show, :edit, :update, :destroy]

  # GET /order_states
  # GET /order_states.json
  def index
    @order_states = OrderState.search(params[:search], params[:page])
  end

  # GET /order_states/1
  # GET /order_states/1.json
  def show
  end

  # GET /order_states/new
  def new
    @order_state = OrderState.new
  end

  # GET /order_states/1/edit
  def edit
  end

  # POST /order_states
  # POST /order_states.json
  def create
    @order_state = OrderState.new(order_state_params)

    respond_to do |format|
      if @order_state.save
        format.html { redirect_to order_states_url, notice: 'Estado de Orden registrado exitosamente.' }
        format.json { render :show, status: :created, location: @order_state }
      else
        format.html { render :new }
        format.json { render json: @order_state.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /order_states/1
  # PATCH/PUT /order_states/1.json
  def update
    respond_to do |format|
      if @order_state.update(order_state_params)
        format.html { redirect_to order_states_url, notice: 'Estado de Orden actualizado exitosamente.' }
        format.json { render :show, status: :ok, location: @order_state }
      else
        format.html { render :edit }
        format.json { render json: @order_state.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /order_states/1
  # DELETE /order_states/1.json
  def destroy
    @order_state.destroy
    respond_to do |format|
      format.html { redirect_to order_states_url, notice: 'Estado de Orden eliminado exitosamente.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_order_state
      @order_state = OrderState.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def order_state_params
      params.require(:order_state).permit(:codigo_Ost, :nombre_Ost, :descripcion_Ost, :estado_Ost)
    end
end
