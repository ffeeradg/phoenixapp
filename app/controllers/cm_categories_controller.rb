class CmCategoriesController < AppController
  before_action :set_cm_category, only: [:show, :edit, :update, :destroy]

  # GET /cm_categories
  # GET /cm_categories.json
  def index
    @cm_categories = CmCategory.search(params[:search], params[:page])
  end

  # GET /cm_categories/1
  # GET /cm_categories/1.json
  def show
  end

  # GET /cm_categories/new
  def new
    @cm_category = CmCategory.new
  end

  # GET /cm_categories/1/edit
  def edit
  end

  # POST /cm_categories
  # POST /cm_categories.json
  def create
    @cm_category = CmCategory.new(cm_category_params)

    respond_to do |format|
      if @cm_category.save
        format.html { redirect_to cm_categories_url, notice: 'Categoria registrada exitosamente.' }
        format.json { render :show, status: :created, location: @cm_category }
      else
        format.html { render :new }
        format.json { render json: @cm_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cm_categories/1
  # PATCH/PUT /cm_categories/1.json
  def update
    respond_to do |format|@cm_categories
      if @cm_category.update(cm_category_params)
        format.html { redirect_to cm_categories_url, notice: 'Categoria actualizada exitosamente.' }
        format.json { render :show, status: :ok, location: @cm_category }
      else
        format.html { render :edit }
        format.json { render json: @cm_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cm_categories/1
  # DELETE /cm_categories/1.json
  def destroy
    @cm_category.destroy
    respond_to do |format|
      format.html { redirect_to cm_categories_url, notice: 'Categoria eliminada exitosamente.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cm_category
      @cm_category = CmCategory.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def cm_category_params
      params.require(:cm_category).permit(:codigo_CM, :nombre_CM, :cp_category_id, :estado_CM)
    end
end
