class AppController < ApplicationController
  before_action :authenticate_user!

  layout :layout_by_resource

  private

  def layout_by_resource
  	if devise_controller?
  		"devise"
  	else
      if current_user == nil
        "application"
      else
        "app"
      end
    end
  end

end