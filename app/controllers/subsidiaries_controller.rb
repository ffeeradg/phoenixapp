class SubsidiariesController < AppController
  before_action :set_subsidiary, only: [:show, :edit, :update, :destroy]

  # GET /subsidiaries
  # GET /subsidiaries.json
  def index
    @subsidiaries = Subsidiary.search(params[:search], params[:page])
  end

  # GET /subsidiaries/1
  # GET /subsidiaries/1.json
  def show
  end

  # GET /subsidiaries/new
  def new
    @subsidiary = Subsidiary.new
  end

  # GET /subsidiaries/1/edit
  def edit
  end

  # POST /subsidiaries
  # POST /subsidiaries.json
  def create
    @subsidiary = Subsidiary.new(subsidiary_params)

    respond_to do |format|
      if @subsidiary.save
        format.html { redirect_to subsidiaries_url, notice: 'Sucursal registrada exitosamente.' }
        format.json { render :show, status: :created, location: @subsidiary }
      else
        format.html { render :new }
        format.json { render json: @subsidiary.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /subsidiaries/1
  # PATCH/PUT /subsidiaries/1.json
  def update
    respond_to do |format|
      if @subsidiary.update(subsidiary_params)
        format.html { redirect_to subsidiaries_url, notice: 'Sucursal actualizada exitosamente.' }
        format.json { render :show, status: :ok, location: @subsidiary }
      else
        format.html { render :edit }
        format.json { render json: @subsidiary.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /subsidiaries/1
  # DELETE /subsidiaries/1.json
  def destroy
    @subsidiary.destroy
    respond_to do |format|
      format.html { redirect_to subsidiaries_url, notice: 'Sucursal eliminada exitosamente.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_subsidiary
      @subsidiary = Subsidiary.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def subsidiary_params
      params.require(:subsidiary).permit(:codigo_Sub, :company_id, :nombre_Sub, :telefono_Sub, :direccion_Sub, :department_id, :municipality_id, :estado_Sub)
    end
end
