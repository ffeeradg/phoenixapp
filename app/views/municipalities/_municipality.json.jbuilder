json.extract! municipality, :id, :codigo_Mun, :nombre_Mun, :descripcion_Mun, :department_id, :estado_Mun, :created_at, :updated_at
json.url municipality_url(municipality, format: :json)
