json.extract! subsidiary, :id, :codigo_Sub, :company_id, :nombre_Sub, :telefono_Sub, :direccion_Sub, :department_id, :municipality_id, :estado_Sub, :created_at, :updated_at
json.url subsidiary_url(subsidiary, format: :json)
