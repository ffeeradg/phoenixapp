json.extract! department, :id, :codigo_Dep, :nombre_Dep, :descripcion_Dep, :estado_Dep, :created_at, :updated_at
json.url department_url(department, format: :json)
