json.extract! cm_category, :id, :codigo_CM, :nombre_CM, :estado_CM, :created_at, :updated_at
json.url cm_category_url(cm_category, format: :json)
