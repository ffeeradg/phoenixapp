json.extract! location, :id, :codigo_Loc, :nombre_Loc, :descripcion_Loc, :subsidiary_id, :estado_Loc, :created_at, :updated_at
json.url location_url(location, format: :json)
