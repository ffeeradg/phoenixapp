json.extract! cp_category, :id, :codigo_CP, :nombre_CP, :estado_CP, :created_at, :updated_at
json.url cp_category_url(cp_category, format: :json)
