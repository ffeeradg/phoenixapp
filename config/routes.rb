Rails.application.routes.draw do

  resources :products
  resources :profiles
  devise_for :users
  resources :locations
  resources :subsidiaries
  resources :cp_categories
  resources :tasks
  resources :cycles
  resources :service_types
  resources :service_states
  resources :order_states
  resources :rols
  resources :companies
  resources :models
  resources :cm_categories
  resources :brands
  resources :municipalities
  resources :makers
  resources :departments

  root :to => 'static_pages#index'
  
  get 'static_pages/about'
  get 'static_pages/contact'
  get 'static_pages/credit'
  get 'static_pages/index'
  get 'static_pages/news_offers'
  get 'static_pages/our_history'
  get 'static_pages/our_mission'
  get 'static_pages/our_vision'
  get 'static_pages/service_backup'
  get 'static_pages/service_licensing'
  get 'static_pages/service_outsourcing'
  get 'static_pages/service_security'
  get 'static_pages/service_telecommunications'
  get 'static_pages/service_uptime'
  post 'static_pages/our_history'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
