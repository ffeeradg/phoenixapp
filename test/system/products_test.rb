require "application_system_test_case"

class ProductsTest < ApplicationSystemTestCase
  setup do
    @product = products(:one)
  end

  test "visiting the index" do
    visit products_url
    assert_selector "h1", text: "Products"
  end

  test "creating a Product" do
    visit products_url
    click_on "New Product"

    fill_in "Cantidad prod", with: @product.cantidad_Prod
    fill_in "Codigo prod", with: @product.codigo_Prod
    fill_in "Descripcion prod", with: @product.descripcion_Prod
    check "Estado0 ord" if @product.estado0_Ord
    fill_in "Location", with: @product.location_id
    fill_in "Model", with: @product.model_id
    click_on "Create Product"

    assert_text "Product was successfully created"
    click_on "Back"
  end

  test "updating a Product" do
    visit products_url
    click_on "Edit", match: :first

    fill_in "Cantidad prod", with: @product.cantidad_Prod
    fill_in "Codigo prod", with: @product.codigo_Prod
    fill_in "Descripcion prod", with: @product.descripcion_Prod
    check "Estado0 ord" if @product.estado0_Ord
    fill_in "Location", with: @product.location_id
    fill_in "Model", with: @product.model_id
    click_on "Update Product"

    assert_text "Product was successfully updated"
    click_on "Back"
  end

  test "destroying a Product" do
    visit products_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Product was successfully destroyed"
  end
end
