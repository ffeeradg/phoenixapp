require "application_system_test_case"

class ProfilesTest < ApplicationSystemTestCase
  setup do
    @profile = profiles(:one)
  end

  test "visiting the index" do
    visit profiles_url
    assert_selector "h1", text: "Profiles"
  end

  test "creating a Profile" do
    visit profiles_url
    click_on "New Profile"

    fill_in "Apellido pro", with: @profile.apellido_Pro
    fill_in "Codigo pro", with: @profile.codigo_Pro
    fill_in "Direccion pro", with: @profile.direccion_Pro
    check "Estado loc" if @profile.estado_Loc
    fill_in "Nombre pro", with: @profile.nombre_Pro
    fill_in "Rol", with: @profile.rol_id
    fill_in "Subsidiary", with: @profile.subsidiary_id
    fill_in "Telefono pro", with: @profile.telefono_Pro
    fill_in "User", with: @profile.user_id
    click_on "Create Profile"

    assert_text "Profile was successfully created"
    click_on "Back"
  end

  test "updating a Profile" do
    visit profiles_url
    click_on "Edit", match: :first

    fill_in "Apellido pro", with: @profile.apellido_Pro
    fill_in "Codigo pro", with: @profile.codigo_Pro
    fill_in "Direccion pro", with: @profile.direccion_Pro
    check "Estado loc" if @profile.estado_Loc
    fill_in "Nombre pro", with: @profile.nombre_Pro
    fill_in "Rol", with: @profile.rol_id
    fill_in "Subsidiary", with: @profile.subsidiary_id
    fill_in "Telefono pro", with: @profile.telefono_Pro
    fill_in "User", with: @profile.user_id
    click_on "Update Profile"

    assert_text "Profile was successfully updated"
    click_on "Back"
  end

  test "destroying a Profile" do
    visit profiles_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Profile was successfully destroyed"
  end
end
