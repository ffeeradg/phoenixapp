require "application_system_test_case"

class CmCategoriesTest < ApplicationSystemTestCase
  setup do
    @cm_category = cm_categories(:one)
  end

  test "visiting the index" do
    visit cm_categories_url
    assert_selector "h1", text: "Cm Categories"
  end

  test "creating a Cm category" do
    visit cm_categories_url
    click_on "New Cm Category"

    fill_in "Codigo cm", with: @cm_category.codigo_CM
    check "Estado cm" if @cm_category.estado_CM
    fill_in "Nombre cm", with: @cm_category.nombre_CM
    click_on "Create Cm category"

    assert_text "Cm category was successfully created"
    click_on "Back"
  end

  test "updating a Cm category" do
    visit cm_categories_url
    click_on "Edit", match: :first

    fill_in "Codigo cm", with: @cm_category.codigo_CM
    check "Estado cm" if @cm_category.estado_CM
    fill_in "Nombre cm", with: @cm_category.nombre_CM
    click_on "Update Cm category"

    assert_text "Cm category was successfully updated"
    click_on "Back"
  end

  test "destroying a Cm category" do
    visit cm_categories_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Cm category was successfully destroyed"
  end
end
