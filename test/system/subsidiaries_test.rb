require "application_system_test_case"

class SubsidiariesTest < ApplicationSystemTestCase
  setup do
    @subsidiary = subsidiaries(:one)
  end

  test "visiting the index" do
    visit subsidiaries_url
    assert_selector "h1", text: "Subsidiaries"
  end

  test "creating a Subsidiary" do
    visit subsidiaries_url
    click_on "New Subsidiary"

    fill_in "Codigo sub", with: @subsidiary.codigo_Sub
    fill_in "Company", with: @subsidiary.company_id
    fill_in "Department", with: @subsidiary.department_id
    fill_in "Direccion sub", with: @subsidiary.direccion_Sub
    check "Estado sub" if @subsidiary.estado_Sub
    fill_in "Municipality", with: @subsidiary.municipality_id
    fill_in "Nombre sub", with: @subsidiary.nombre_Sub
    fill_in "Telefono sub", with: @subsidiary.telefono_Sub
    click_on "Create Subsidiary"

    assert_text "Subsidiary was successfully created"
    click_on "Back"
  end

  test "updating a Subsidiary" do
    visit subsidiaries_url
    click_on "Edit", match: :first

    fill_in "Codigo sub", with: @subsidiary.codigo_Sub
    fill_in "Company", with: @subsidiary.company_id
    fill_in "Department", with: @subsidiary.department_id
    fill_in "Direccion sub", with: @subsidiary.direccion_Sub
    check "Estado sub" if @subsidiary.estado_Sub
    fill_in "Municipality", with: @subsidiary.municipality_id
    fill_in "Nombre sub", with: @subsidiary.nombre_Sub
    fill_in "Telefono sub", with: @subsidiary.telefono_Sub
    click_on "Update Subsidiary"

    assert_text "Subsidiary was successfully updated"
    click_on "Back"
  end

  test "destroying a Subsidiary" do
    visit subsidiaries_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Subsidiary was successfully destroyed"
  end
end
