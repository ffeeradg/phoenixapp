require "application_system_test_case"

class ServiceStatesTest < ApplicationSystemTestCase
  setup do
    @service_state = service_states(:one)
  end

  test "visiting the index" do
    visit service_states_url
    assert_selector "h1", text: "Service States"
  end

  test "creating a Service state" do
    visit service_states_url
    click_on "New Service State"

    fill_in "Codigo sta", with: @service_state.codigo_Sta
    fill_in "Descripcion sta", with: @service_state.descripcion_Sta
    check "Estado sta" if @service_state.estado_Sta
    fill_in "Nombre sta", with: @service_state.nombre_Sta
    click_on "Create Service state"

    assert_text "Service state was successfully created"
    click_on "Back"
  end

  test "updating a Service state" do
    visit service_states_url
    click_on "Edit", match: :first

    fill_in "Codigo sta", with: @service_state.codigo_Sta
    fill_in "Descripcion sta", with: @service_state.descripcion_Sta
    check "Estado sta" if @service_state.estado_Sta
    fill_in "Nombre sta", with: @service_state.nombre_Sta
    click_on "Update Service state"

    assert_text "Service state was successfully updated"
    click_on "Back"
  end

  test "destroying a Service state" do
    visit service_states_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Service state was successfully destroyed"
  end
end
