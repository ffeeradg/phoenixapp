require "application_system_test_case"

class CpCategoriesTest < ApplicationSystemTestCase
  setup do
    @cp_category = cp_categories(:one)
  end

  test "visiting the index" do
    visit cp_categories_url
    assert_selector "h1", text: "Cp Categories"
  end

  test "creating a Cp category" do
    visit cp_categories_url
    click_on "New Cp Category"

    fill_in "Codigo cp", with: @cp_category.codigo_CP
    check "Estado cp" if @cp_category.estado_CP
    fill_in "Nombre cp", with: @cp_category.nombre_CP
    click_on "Create Cp category"

    assert_text "Cp category was successfully created"
    click_on "Back"
  end

  test "updating a Cp category" do
    visit cp_categories_url
    click_on "Edit", match: :first

    fill_in "Codigo cp", with: @cp_category.codigo_CP
    check "Estado cp" if @cp_category.estado_CP
    fill_in "Nombre cp", with: @cp_category.nombre_CP
    click_on "Update Cp category"

    assert_text "Cp category was successfully updated"
    click_on "Back"
  end

  test "destroying a Cp category" do
    visit cp_categories_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Cp category was successfully destroyed"
  end
end
