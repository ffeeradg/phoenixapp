require 'test_helper'

class CpCategoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @cp_category = cp_categories(:one)
  end

  test "should get index" do
    get cp_categories_url
    assert_response :success
  end

  test "should get new" do
    get new_cp_category_url
    assert_response :success
  end

  test "should create cp_category" do
    assert_difference('CpCategory.count') do
      post cp_categories_url, params: { cp_category: { codigo_CP: @cp_category.codigo_CP, estado_CP: @cp_category.estado_CP, nombre_CP: @cp_category.nombre_CP } }
    end

    assert_redirected_to cp_category_url(CpCategory.last)
  end

  test "should show cp_category" do
    get cp_category_url(@cp_category)
    assert_response :success
  end

  test "should get edit" do
    get edit_cp_category_url(@cp_category)
    assert_response :success
  end

  test "should update cp_category" do
    patch cp_category_url(@cp_category), params: { cp_category: { codigo_CP: @cp_category.codigo_CP, estado_CP: @cp_category.estado_CP, nombre_CP: @cp_category.nombre_CP } }
    assert_redirected_to cp_category_url(@cp_category)
  end

  test "should destroy cp_category" do
    assert_difference('CpCategory.count', -1) do
      delete cp_category_url(@cp_category)
    end

    assert_redirected_to cp_categories_url
  end
end
