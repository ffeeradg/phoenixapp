require 'test_helper'

class CmCategoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @cm_category = cm_categories(:one)
  end

  test "should get index" do
    get cm_categories_url
    assert_response :success
  end

  test "should get new" do
    get new_cm_category_url
    assert_response :success
  end

  test "should create cm_category" do
    assert_difference('CmCategory.count') do
      post cm_categories_url, params: { cm_category: { codigo_CM: @cm_category.codigo_CM, estado_CM: @cm_category.estado_CM, nombre_CM: @cm_category.nombre_CM } }
    end

    assert_redirected_to cm_category_url(CmCategory.last)
  end

  test "should show cm_category" do
    get cm_category_url(@cm_category)
    assert_response :success
  end

  test "should get edit" do
    get edit_cm_category_url(@cm_category)
    assert_response :success
  end

  test "should update cm_category" do
    patch cm_category_url(@cm_category), params: { cm_category: { codigo_CM: @cm_category.codigo_CM, estado_CM: @cm_category.estado_CM, nombre_CM: @cm_category.nombre_CM } }
    assert_redirected_to cm_category_url(@cm_category)
  end

  test "should destroy cm_category" do
    assert_difference('CmCategory.count', -1) do
      delete cm_category_url(@cm_category)
    end

    assert_redirected_to cm_categories_url
  end
end
