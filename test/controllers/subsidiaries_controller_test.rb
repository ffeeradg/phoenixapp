require 'test_helper'

class SubsidiariesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @subsidiary = subsidiaries(:one)
  end

  test "should get index" do
    get subsidiaries_url
    assert_response :success
  end

  test "should get new" do
    get new_subsidiary_url
    assert_response :success
  end

  test "should create subsidiary" do
    assert_difference('Subsidiary.count') do
      post subsidiaries_url, params: { subsidiary: { codigo_Sub: @subsidiary.codigo_Sub, company_id: @subsidiary.company_id, department_id: @subsidiary.department_id, direccion_Sub: @subsidiary.direccion_Sub, estado_Sub: @subsidiary.estado_Sub, municipality_id: @subsidiary.municipality_id, nombre_Sub: @subsidiary.nombre_Sub, telefono_Sub: @subsidiary.telefono_Sub } }
    end

    assert_redirected_to subsidiary_url(Subsidiary.last)
  end

  test "should show subsidiary" do
    get subsidiary_url(@subsidiary)
    assert_response :success
  end

  test "should get edit" do
    get edit_subsidiary_url(@subsidiary)
    assert_response :success
  end

  test "should update subsidiary" do
    patch subsidiary_url(@subsidiary), params: { subsidiary: { codigo_Sub: @subsidiary.codigo_Sub, company_id: @subsidiary.company_id, department_id: @subsidiary.department_id, direccion_Sub: @subsidiary.direccion_Sub, estado_Sub: @subsidiary.estado_Sub, municipality_id: @subsidiary.municipality_id, nombre_Sub: @subsidiary.nombre_Sub, telefono_Sub: @subsidiary.telefono_Sub } }
    assert_redirected_to subsidiary_url(@subsidiary)
  end

  test "should destroy subsidiary" do
    assert_difference('Subsidiary.count', -1) do
      delete subsidiary_url(@subsidiary)
    end

    assert_redirected_to subsidiaries_url
  end
end
