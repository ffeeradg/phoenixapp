require 'test_helper'

class MakersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @maker = makers(:one)
  end

  test "should get index" do
    get makers_url
    assert_response :success
  end

  test "should get new" do
    get new_maker_url
    assert_response :success
  end

  test "should create maker" do
    assert_difference('Maker.count') do
      post makers_url, params: { maker: { codigo_Mak: @maker.codigo_Mak, estado_Mak: @maker.estado_Mak, nombre_Mak: @maker.nombre_Mak } }
    end

    assert_redirected_to maker_url(Maker.last)
  end

  test "should show maker" do
    get maker_url(@maker)
    assert_response :success
  end

  test "should get edit" do
    get edit_maker_url(@maker)
    assert_response :success
  end

  test "should update maker" do
    patch maker_url(@maker), params: { maker: { codigo_Mak: @maker.codigo_Mak, estado_Mak: @maker.estado_Mak, nombre_Mak: @maker.nombre_Mak } }
    assert_redirected_to maker_url(@maker)
  end

  test "should destroy maker" do
    assert_difference('Maker.count', -1) do
      delete maker_url(@maker)
    end

    assert_redirected_to makers_url
  end
end
