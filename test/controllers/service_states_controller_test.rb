require 'test_helper'

class ServiceStatesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @service_state = service_states(:one)
  end

  test "should get index" do
    get service_states_url
    assert_response :success
  end

  test "should get new" do
    get new_service_state_url
    assert_response :success
  end

  test "should create service_state" do
    assert_difference('ServiceState.count') do
      post service_states_url, params: { service_state: { codigo_Sta: @service_state.codigo_Sta, descripcion_Sta: @service_state.descripcion_Sta, estado_Sta: @service_state.estado_Sta, nombre_Sta: @service_state.nombre_Sta } }
    end

    assert_redirected_to service_state_url(ServiceState.last)
  end

  test "should show service_state" do
    get service_state_url(@service_state)
    assert_response :success
  end

  test "should get edit" do
    get edit_service_state_url(@service_state)
    assert_response :success
  end

  test "should update service_state" do
    patch service_state_url(@service_state), params: { service_state: { codigo_Sta: @service_state.codigo_Sta, descripcion_Sta: @service_state.descripcion_Sta, estado_Sta: @service_state.estado_Sta, nombre_Sta: @service_state.nombre_Sta } }
    assert_redirected_to service_state_url(@service_state)
  end

  test "should destroy service_state" do
    assert_difference('ServiceState.count', -1) do
      delete service_state_url(@service_state)
    end

    assert_redirected_to service_states_url
  end
end
